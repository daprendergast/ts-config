export default interface v1_2 {
    /** a valid version - can only be 1.2*/
    version: Version
}
type Version = "1.2"